# Invokes a Cmd.exe shell script and updates the environment.
function Invoke-CmdScript {
  param(
    [String] $scriptName
  )
  $cmdLine = """$scriptName"" $args & set"
  & $Env:SystemRoot\system32\cmd.exe /c $cmdLine |
  select-string '^([^=]*)=(.*)$' | foreach-object {
    $varName = $_.Matches[0].Groups[1].Value
    $varValue = $_.Matches[0].Groups[2].Value
    set-item Env:$varName $varValue
  }
}
# Set Qt version
$QT_VERSION="5.9.1"

$PYQT_VERSION="5.9"
$SIP_VERSION="4.19.3"
$PYTHON="c:\\python36"

$WORKING_FOLDER=(Get-Location).path
$QTDIR="$WORKING_FOLDER\qt-$QT_VERSION"

# Set up PATH
$env:Path += ";C:\Program Files (x86)\gnuwin32\bin;C:\ProgramData\chocolatey\bin;$QTDIR\qtbase\bin"

# Fetch SIP source code
C:\ProgramData\chocolatey\bin\curl.exe -L -O -k http://downloads.sourceforge.net/project/pyqt/sip/sip-$SIP_VERSION/sip-$SIP_VERSION.zip
# Fetch PyQt5 source code
C:\ProgramData\chocolatey\bin\curl.exe -L -O -k http://downloads.sourceforge.net/project/pyqt/PyQt5/PyQt-$PYQT_VERSION/PyQt5_gpl-$PYQT_VERSION.zip

# Build sip
# Extract zip if needed
7z x sip-$SIP_VERSION.zip
cd sip-$SIP_VERSION
&"$env:PYTHON\\python.exe" configure.py
Invoke-CmdScript "$QTDIR\bin\qtenv2.bat"
Invoke-CmdScript "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x86
cd $WORKING_FOLDER\sip-$SIP_VERSION
nmake
cd ..

# Build PyQt5
# Extract zip if needed
7z x PyQt5_gpl-$PYQT_VERSION.zip
cd PyQt5_gpl-$PYQT_VERSION
&"$env:PYTHON\python.exe" configure.py --help
&"$env:PYTHON\python.exe" configure.py --verbose --disable QtNfc --confirm-license --sip %PYTHON%\\sip.exe
nmake