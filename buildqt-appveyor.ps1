# Invokes a Cmd.exe shell script and updates the environment.
function Invoke-CmdScript {
  param(
    [String] $scriptName
  )
  $cmdLine = """$scriptName"" $args & set"
  & $Env:SystemRoot\system32\cmd.exe /c $cmdLine |
  select-string '^([^=]*)=(.*)$' | foreach-object {
    $varName = $_.Matches[0].Groups[1].Value
    $varValue = $_.Matches[0].Groups[2].Value
    set-item Env:$varName $varValue
  }
}

# Set Qt version
$QT_VERSION_SHORT="5.10"
$QT_VERSION="5.10.1"

# Download Qt
C:\ProgramData\chocolatey\bin\curl.exe -L -O -k https://download.qt.io/official_releases/qt/$QT_VERSION_SHORT/$QT_VERSION/single/qt-everywhere-src-$QT_VERSION.zip

# Build QT!

# Extract zip if needed
7z x qt-everywhere-src-$QT_VERSION.zip
mv qt-everywhere-src-$QT_VERSION qt-$QT_VERSION
cd qt-$QT_VERSION
Invoke-CmdScript "C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat" x86
&".\\configure" -release -nomake examples -nomake tests -opensource -confirm-license -platform win32-msvc2015 -proprietary-codecs

$QTDIR="C:\Qt\$QT_VERSION\msvc2015"
$env:Path += ";$QTDIR\bin"
cd qtwebengine
ls
&"$QTDIR\\bin\\qmake.exe" CONFIG+="proprietary-codecs" WEBENGINE_CONFIG+="use_proprietary_codecs" qtwebengine.pro
jom
cd ..

