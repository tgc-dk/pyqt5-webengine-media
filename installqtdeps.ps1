# Install stuff needed for Qt compiling! Run as admin!
# Chokolatey is required! https://chocolatey.org/

# Set up PATH
$env:Path += ";C:\ProgramData\chocolatey\bin"

# Install curl so that we can download stuff!
choco install -y --force curl

# Install curl so that we can download stuff!
#choco install -y --force 7zip

# Install jom to enable building in parallel
choco install -y --force jom

# Install activeperl needed when building Qt
#choco install -y --force activeperl

# Install flex and bison for windows, needed for building webengine
choco install -y --force winflexbison
cp C:\ProgramData\chocolatey\bin\win_flex.exe C:\ProgramData\chocolatey\bin\flex.exe
cp C:\ProgramData\chocolatey\bin\win_bison.exe C:\ProgramData\chocolatey\bin\bison.exe

# Install gperf for windows, needed for building webengine
C:\ProgramData\chocolatey\bin\curl.exe -k -L -O http://downloads.sourceforge.net/project/gnuwin32/gperf/3.0.1/gperf-3.0.1.exe
&".\\gperf-3.0.1.exe" /VERYSILENT

choco install -y --force ninja re2c